package com.example09;

import org.junit.jupiter.api.Test;

import java.util.List;

public class DeveloperServiceTest {

    @Test
    void shouldLoadDevelopers() {
        DeveloperService developerService = new DeveloperService();

        List<Developer> developers = developerService.loadDevelopers();

        System.out.println(developers);
    }
}
