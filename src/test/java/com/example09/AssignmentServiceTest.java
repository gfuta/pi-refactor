package com.example09;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;

/**
 * http://dominisz.pl
 * 17.05.2018
 */
public class AssignmentServiceTest {

    private Gson gson = new Gson();

    @Test
    void saveDevelopers() {
        List<Developer> developers = new ArrayList<>();

        developers.add(new Developer("0001", "Jacob", "team-01"));
        developers.add(new Developer("0002", "Chloe", "team-01"));
        developers.add(new Developer("0003", "Michael", "team-01"));
        developers.add(new Developer("0004", "Emily", "team-01"));
        developers.add(new Developer("0005", "Shawn", "A Team"));
        developers.add(new Developer("0006", "Emma", "A Team"));
        developers.add(new Developer("0007", "Daniel", "A Team"));
        developers.add(new Developer("0008", "Jennifer", "A Team"));

        String json = gson.toJson(developers.toArray());

        System.out.println(json);

        List<Project> projects = new ArrayList<>();

        projects.add(new Project("/projects/0001", "A Project", LocalDate.of(2015, 1, 1), LocalDate.of(2020, 12, 31)));
        projects.add(new Project("/projects/0002", "Project Euler", LocalDate.of(2017, 5, 10), null));
        projects.add(new Project("/projects/0003", "Clouds", LocalDate.of(2018, 7, 1), LocalDate.of(2019, 6, 30)));

        json = gson.toJson(projects);

        System.out.println(json);


        Set<Assignment> assignments = new HashSet<>();

        assignments.add(new Assignment(projects.get(0), developers.get(4)));
        assignments.add(new Assignment(projects.get(0), developers.get(5)));
        assignments.add(new Assignment(projects.get(0), developers.get(6)));
        assignments.add(new Assignment(projects.get(0), developers.get(7)));

        assignments.add(new Assignment(projects.get(1), developers.get(0)));
        assignments.add(new Assignment(projects.get(1), developers.get(1)));
        assignments.add(new Assignment(projects.get(1), developers.get(2)));
        assignments.add(new Assignment(projects.get(1), developers.get(3)));

        assignments.add(new Assignment(projects.get(2), developers.get(2)));
        assignments.add(new Assignment(projects.get(2), developers.get(4)));
        assignments.add(new Assignment(projects.get(2), developers.get(6)));

        json = gson.toJson(assignments);

        System.out.println(json);
    }

    @Test
    void saveProjects() {
        List<Project> projects = new ArrayList<>();

        projects.add(new Project("/projects/0001", "A Project", LocalDate.of(2015, 1, 1), LocalDate.of(2020, 12, 31)));
        projects.add(new Project("/projects/0002", "Project Euler", LocalDate.of(2017, 5, 10), null));
        projects.add(new Project("/projects/0003", "Clouds", LocalDate.of(2018, 7, 1), LocalDate.of(2019, 6, 30)));

        String json = gson.toJson(projects);

        System.out.println(json);
    }

    @Test
    void saveAssignments() {
        List<Developer> developers = new ArrayList<>();

        developers.add(new Developer("/developers/0001", "Jacob", "team-01"));
        developers.add(new Developer("/developers/0002", "Chloe", "team-01"));
        developers.add(new Developer("/developers/0003", "Michael", "team-01"));
        developers.add(new Developer("/developers/0004", "Emily", "team-01"));
        developers.add(new Developer("/developers/0005", "Shawn", "A Team"));
        developers.add(new Developer("/developers/0006", "Emma", "A Team"));
        developers.add(new Developer("/developers/0007", "Daniel", "A Team"));
        developers.add(new Developer("/developers/0008", "Jennifer", "A Team"));

        String json = gson.toJson(developers.toArray());

        System.out.println(json);

        List<Project> projects = new ArrayList<>();

        projects.add(new Project("/projects/0001", "A Project", LocalDate.of(2015, 1, 1), LocalDate.of(2020, 12, 31)));
        projects.add(new Project("/projects/0002", "Project Euler", LocalDate.of(2017, 5, 10), null));
        projects.add(new Project("/projects/0003", "Clouds", LocalDate.of(2018, 7, 1), LocalDate.of(2019, 6, 30)));

        json = gson.toJson(projects);

        System.out.println(json);


        Set<Assignment> assignments = new HashSet<>();

        assignments.add(new Assignment(projects.get(0), developers.get(4)));
        assignments.add(new Assignment(projects.get(0), developers.get(5)));
        assignments.add(new Assignment(projects.get(0), developers.get(6)));
        assignments.add(new Assignment(projects.get(0), developers.get(7)));

        assignments.add(new Assignment(projects.get(1), developers.get(0)));
        assignments.add(new Assignment(projects.get(1), developers.get(1)));
        assignments.add(new Assignment(projects.get(1), developers.get(2)));
        assignments.add(new Assignment(projects.get(1), developers.get(3)));

        assignments.add(new Assignment(projects.get(2), developers.get(2)));
        assignments.add(new Assignment(projects.get(2), developers.get(4)));
        assignments.add(new Assignment(projects.get(2), developers.get(6)));


//        Map<Project, Set<Assignment>> map = new HashMap<>();
//
//        assignments.forEach(assignment ->
//                map.computeIfAbsent(assignment.getProject(), key -> new HashSet<>())
//                        .add(assignment));

        json = gson.toJson(assignments);

        System.out.println(json);
    }

    @Test
    void shouldLoadAssignments() {
        AssignmentService assignmentService = new AssignmentService();
        List<Assignment> assignments = assignmentService.loadAssignments();
        System.out.println(assignments);
    }

}
