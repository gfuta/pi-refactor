package com.example09;

import org.junit.jupiter.api.Test;

import java.util.List;

public class ProjectServiceTest {

    @Test
    void shouldLoadProjects() {
        ProjectService projectService = new ProjectService();
        List<Project> projects = projectService.loadProjects();
        System.out.println(projects);
    }

}
