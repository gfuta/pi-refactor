package com.example09;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AssignmentController {

    private List<Project> projects = new ArrayList<>();
    private List<Developer> developers = new ArrayList<>();

    private Map<Project, Set<Assignment>> projectAssignments = new HashMap<>();

    private AssignmentService assignmentService = new AssignmentService();
    private ProjectService projectService = new ProjectService();
    private DeveloperService developerService = new DeveloperService();

    public void loadProjects() {
        projects = projectService.loadProjects();
    }

    public void loadDevelopers() {
        developers = developerService.loadDevelopers();
    }

    public void loadAssignments() {
        List<Assignment> assignments = assignmentService.loadAssignments();

        assignments.forEach(assignment ->
                projectAssignments
                        .computeIfAbsent(assignment.getProject(), key -> new HashSet<>())
                        .add(assignment)
        );
    }

    public void initializeProjectAssignments() {
        Map<String, Developer> developerMap = developers.stream()
                .collect(Collectors.toMap(Developer::getId, Function.identity()));

        Map<String, Project> projectMap = projects.stream()
                .collect(Collectors.toMap(Project::getId, Function.identity()));

        projectAssignments.keySet().stream().collect(Collectors.toList()).forEach(project ->
                projectAssignments.remove(project)
                        .forEach(assignment ->
                                projectAssignments.computeIfAbsent(projectMap.get(assignment.getProject().getId()), key -> new HashSet<>())
                                        .add(assignment
                                                .setProject(projectMap.get(assignment.getProject().getId()))
                                                .setDeveloper(developerMap.get(assignment.getDeveloper().getId()))))
        );
    }
}
