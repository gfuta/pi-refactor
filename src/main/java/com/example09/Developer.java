package com.example09;

import lombok.Data;

@Data
public class Developer {

    private final String id;
    private final String name;
    private final String team;

    public Developer(String id, String name, String team) {
        this.id = id;
        this.name = null;
        this.team = null;
//        this.name = name;
//        this.team = team;
    }

}
