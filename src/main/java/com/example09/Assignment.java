package com.example09;

/**
 * http://dominisz.pl
 * 12.05.2018
 */
public class Assignment {

    private Project project;
    private Developer developer;

    public Assignment(Project project, Developer developer) {
        this.project = project;
        this.developer = developer;
    }

    public Project getProject() {
        return project;
    }

    public Assignment setProject(Project project) {
        this.project = project;
        return this;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public Assignment setDeveloper(Developer developer) {
        this.developer = developer;
        return this;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "project=" + project +
                ", developer=" + developer +
                '}';
    }
}
