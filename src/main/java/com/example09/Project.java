package com.example09;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Project {

    private final String id;
    private final String name;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public Project(String id, String name, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.name = null;
        this.startDate = null;
        this.endDate = null;
//        this.name = name;
//        this.startDate = startDate;
//        this.endDate = endDate;
    }

}
