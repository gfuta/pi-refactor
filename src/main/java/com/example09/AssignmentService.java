package com.example09;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class AssignmentService {

    private static String ASSIGNMENTS = "[{\"project\": {\"id\": \"/projects/0002\"}, \"developer\": {\"id\": \"/developers/0001\"}}, {\"project\": {\"id\": \"/projects/0001\"}, \"developer\": {\"id\": \"/developers/0007\"}}, {\"project\": {\"id\": \"/projects/0003\"}, \"developer\": {\"id\": \"/developers/0007\"}}, {\"project\": {\"id\": \"/projects/0002\"}, \"developer\": {\"id\": \"/developers/0003\"}}, {\"project\": {\"id\": \"/projects/0003\"}, \"developer\": {\"id\": \"/developers/0005\"}}, {\"project\": {\"id\": \"/projects/0001\"}, \"developer\": {\"id\": \"/developers/0005\"}}, {\"project\": {\"id\": \"/projects/0003\"}, \"developer\": {\"id\": \"/developers/0003\"}}, {\"project\": {\"id\": \"/projects/0001\"}, \"developer\": {\"id\": \"/developers/0006\"}}, {\"project\": {\"id\": \"/projects/0002\"}, \"developer\": {\"id\": \"/developers/0004\"}}, {\"project\": {\"id\": \"/projects/0002\"}, \"developer\": {\"id\": \"/developers/0002\"}}, {\"project\": {\"id\": \"/projects/0001\"}, \"developer\": {\"id\": \"/developers/0008\"}}]";

    public List<Assignment> loadAssignments() {
        Gson gson = new Gson();
        Assignment[] assignments = gson.fromJson(ASSIGNMENTS, Assignment[].class);
        return Arrays.asList(assignments);
    }

}
