package com.example07;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class ImprovedEditor {

    private Map<EditorMode, Consumer<EditorEvent>> actions;
    private EditorMode editorMode;
    private List<EditorEvent> eventHistory;

    public ImprovedEditor() {
        editorMode = EditorMode.DRAW_MODE;
        eventHistory = new ArrayList<>();

        initializeActions();
    }

    private void initializeActions() {
        actions = new HashMap<>();
        actions.put(EditorMode.DRAW_MODE, new ActionDraw());
        actions.put(EditorMode.MODIFY_MODE, new ActionModify());
        actions.put(EditorMode.ADD_MODE, new ActionAdd());
        actions.put(EditorMode.DELETE_MODE, new ActionJoin());
        actions.put(EditorMode.JOIN_MODE, new ActionJoin());
        actions.put(EditorMode.CUT_MODE, new ActionCut());
    }

    public boolean isDrawMode() {
        return EditorMode.DRAW_MODE.equals(editorMode);
    }

    public void setDrawMode() {
        editorMode = EditorMode.DRAW_MODE;
    }

    public boolean isModifyMode() {
        return EditorMode.MODIFY_MODE.equals(editorMode);
    }

    public void setModifyMode() {
        editorMode = EditorMode.MODIFY_MODE;
    }

    public boolean isAddMode() {
        return EditorMode.ADD_MODE.equals(editorMode);
    }

    public void setAddMode() {
        editorMode = EditorMode.ADD_MODE;
    }

    public boolean isDeleteMode() {
        return EditorMode.DELETE_MODE.equals(editorMode);
    }

    public void setDeleteMode() {
        editorMode = EditorMode.DELETE_MODE;
    }

    public boolean isJoinMode() {
        return EditorMode.JOIN_MODE.equals(editorMode);
    }

    public void setJoinMode() {
        editorMode = EditorMode.JOIN_MODE;
    }

    public boolean isCutMode() {
        return EditorMode.CUT_MODE.equals(editorMode);
    }

    public void setCutMode() {
        editorMode = EditorMode.CUT_MODE;
    }

    public List<EditorEvent> getEventHistory() {
        return eventHistory;
    }

    public void action(EditorEvent editorEvent) {
        eventHistory.add(editorEvent);

        actions.get(editorMode).accept(editorEvent);
    }

}
