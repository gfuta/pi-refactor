# Warsztaty refaktoringu kodu - poziom podstawowy

Repozytorium zawiera kod bazy kod źrółowy do ćwiczeń z refaktoringu kodu.

## Wymagania wstępne

* Java 1.8+
* Gradle (we are using version 4.7)
* Mózg, lub dwa. Myślącze, otwarte - mile widziane.

[Pobierz](https://gradle.org/next-steps/?version=4.7&format=bin) Gradle. Rozpakuj pobraną zawartość do (np.) `c:\gradle`
lub wybranego przez Ciebie katalogu. Skonfiguruj zmienne środowiskowe. Dodaj zmienną `GRADLE_HOME` oraz
`GRADLE_HOME/bin` do zmiennej `PATH`.

Szczegółowe informacje zawarte są na stronie: https://docs.gradle.org/current/userguide/installation.html.

## Zapamiętaj!

Każda próba refaktoringu wymaga napisania testów. **PRZED NIM!** Każdy refaktoring może przyczynić się do nieoczekiwaych
zmian w sposobie dziłania aplikacji. Właściwie zaplanowane testy mogą zapobiec regresowi bądź błędom w dzialaniu po zmianach.

### Cwiczenia 

#### Zadanie 01

[Przeczytaj opis zadania](src/main/java/com/example01/README.md)
#### Zadanie 02

Zmień implementację `PowiadomienieORozprawieService.generujDokument()` w taki sposób, aby była barzdiej cztelna
i ustrkturalizowana.

#### Zadanie 03
#### Zadanie 04

Zmień implementację motody `PersonFilterService.filterBySurname()` zamieniając pętle na strumienie i wyrażenia lambda.

A może trochę bardziej skomplikowane zdanie? Spróbuj zmienić implementację`PersonFilterService.getCustomersOfGivenItem()`.

#### Zadanie 06

Try to refacotr method `DiscountCalculatorService.calculateDiscount()`. What design patterns can you use?

Can you refactor classes in `com.example06.model`?

How to introduce total discount to the overall amount of order?
